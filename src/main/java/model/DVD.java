package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@NoArgsConstructor
@Entity
@Setter
@Getter
@ToString(callSuper = true)
/**
 * Class for storing DVD´s in the Database
 * DVD is a child of items and extends his attributes
 */
public class DVD extends Item {

    /**
     * Method to extract all the involved Persons
     * @param element the xml element to get all the Persons
     * @param person Role of the Person
     * @return List of all people which fits the criteria role
     */
    private List<String> getPersons(Element element, String person) {
        List<String> persons = new ArrayList<>();
        NodeList nodeList = element.getElementsByTagName(person);
        for (int i = 0; i < nodeList.getLength(); i++) {
            persons.add(nodeList.item(i).getTextContent().replace("\n", ""));
        }
        return persons;
    }

    /**
     * List of all studios from DVD in a separate table
     */
    @ElementCollection
    private List<String> studios = new ArrayList<>();

    /**
     * List of all actors from DVD in a separate table
     */
    @ElementCollection
    private List<String> actors = new ArrayList<>();

    /**
     * List of all directors from DVD in a separate table
     */
    @ElementCollection
    private List<String> directors = new ArrayList<>();

    /**
     * List of all creators from DVD in a seperate table
     */
    @ElementCollection
    private List<String> creators = new ArrayList<>();

    // DVDSpecific attributes


    private String aspectratio;

    private String format;

    private Date releasedate;

    private int runtime;

    private long upcValue;

    private int regionCode;

    private int theatrRelease;

}
