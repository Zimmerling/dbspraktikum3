package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@ToString

/**
 * Class for storing categories in the Database
 */

public class Categorie {
    @Id
    @ToString.Exclude
    @GeneratedValue()
    private long id;

    public Categorie(String name) {
        this.name = name;
    }

    private String name;

    /**
     * Set of all subcategories in a separate table
     * <Categorie>--1--is--m--<Categorie>
     */
    @OneToMany
    @ToString.Exclude
    private Set<Categorie> subcategories = new HashSet();

    /**
     * Set of all items from book, which are in the categorie, stored in a separate table
     * <Item>--m--is--m--<Item>
     */
    @ManyToMany
    @ToString.Exclude
    private Set<Item> items = new HashSet<>();

    private Boolean mainCategorie = false;

    public String toTreeString(int i) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int a = 0; a < i; a++) {
            stringBuilder.append(" ");
        }
        stringBuilder.append("|--->Category Name: ").append(name).append("\n");
        for (Categorie s : subcategories) {
            stringBuilder.append(s.toTreeString(i + 3));
        }
        return stringBuilder.toString();
    }

}
