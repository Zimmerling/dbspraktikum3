package model;

import com.sun.istack.NotNull;
import lombok.*;
import org.w3c.dom.Element;

import javax.persistence.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
/**
 * Using this strategy, each class in the hierarchy is mapped to its table
 * The only column which repeatably appears in all tables is the identifier
 */
@Inheritance(strategy = InheritanceType.JOINED)

/**
 * Class for Storing the main attributes (asin, ean, salerank, title) each item has
 * Is the parent class of book, dvd and music
 */
public class Item {

    /**
     * The identifier of Item
     */
    @Id
    @ToString.Exclude
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true)
    private String asin;

    @NotNull
    private String title;

    @NotNull
    private String pgroup;

    private int salerank;

    private String ean;

    private String img;

    private float rating;

    /**
     * Set of all items with their similar Item
     * The attribute similar is an ManyToMany Relationship with itself
     * <Item>--m--is--n--<Item>
     */
    @ToString.Exclude
    @ManyToMany
    private Set<Item> similars = new HashSet<>();


}
