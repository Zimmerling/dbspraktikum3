package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Entity
@Setter
@Getter
@ToString(callSuper = true)
/**
 * Class for storing the entity Music in the Database
 * Music is a child of items and extends his attributes
 */
public class Music extends Item {

    /**
     * Method to extract all the involved Persons
     * @param element the xml element to get all the Persons
     * @param person Role of the Person
     * @return List of all people which fits the criteria role
     */
    private List<String> getPersons(Element element, String person) {
        List<String> persons = new ArrayList<>();
        NodeList nodeList = element.getElementsByTagName(person);
        for (int i = 0; i < nodeList.getLength(); i++) {
            persons.add(nodeList.item(i).getTextContent().replace("\n", ""));
        }
        return persons;
    }

    /**
     * List of all listed items in listmania in a separate table
     */
    @ElementCollection()
    private List<String> listmania = new ArrayList<>();

    /**
     * List of all artist from Music in a separate table
     */
    @ElementCollection()
    private List<String> artist = new ArrayList<>();

    /**
     * List of all artist from Music in a separate table
     */
    @ElementCollection()
    private List<String> creators = new ArrayList<>();

    /**
     * List of all labels from Music in a separate table
     */
    @ElementCollection()
    private List<String> labels=new ArrayList<>();

    /**
     * List of all listed tracks from Music in a separate table
     */
    @ElementCollection()
    private List<String> tracks=new ArrayList<>();

    // Musicspecific attributes
    private String binding;

    private String format;

    private int numDisc;

    private Date date;

    private long upc;

}
