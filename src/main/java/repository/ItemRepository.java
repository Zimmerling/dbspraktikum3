package repository;

import adapter.HibernateAdapter;
import model.Book;
import model.DVD;
import model.Item;
import model.Music;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ItemRepository {


    public Item getProduct(long id) {
        Session session = HibernateAdapter.getSession();
        try {
            switch (session.get(Item.class, id).getPgroup().toLowerCase()) {
                case "book":
                    return session.get(Book.class, id);
                case "dvd":
                    return session.get(DVD.class, id);
                case "music":
                    return session.get(Music.class, id);
                default:
                    return null;
            }
        } catch (NullPointerException nullPointerException) {
            return null;
        }
    }

    public List<Item> getProducts(String pattern) {
        Session session = HibernateAdapter.getSession();
        pattern = "%" + pattern + "%";
        Query<Item> query = session.createQuery("FROM Item WHERE title LIKE  :titel", Item.class);
        query.setParameter("titel", pattern);
        return query.list();


    }

    public List<Item> getProductsByCategoryId(long id) {
        Session session = HibernateAdapter.getSession();
        Query<Item> query = session.createQuery("SELECT i FROM Categorie c INNER JOIN c.items i WHERE c.id =:id", Item.class);
        query.setParameter("id", id);
        return query.list();
    }

    public List<Item> getTopProducts() {
        Session session = HibernateAdapter.getSession();
        Query<Item> query = session.createQuery("SELECT i FROM  Item i WHERE i.salerank > 0 ORDER BY i.rating desc, i.salerank", Item.class);
        return query.list();
    }

    public List<Item> getCheaperItems(long id) {
        Session session = HibernateAdapter.getSession();
        Item item = getProduct(id);
        Query<Double> queryGetPrice = session.createQuery("Select MIN(s.price * s.mult) from Stock s where s.item = :item", Double.class);
        queryGetPrice.setParameter("item", item);
        Double minPrice = queryGetPrice.getSingleResult();
        if (minPrice == null) return null;
        Query<Item> queryGetCheaper = session.createQuery("select s.item from Stock s where (s.price * s.mult) < :price");
        return queryGetCheaper.setParameter("price", minPrice).list();
    }

    public void setRatingAvgByItem(float avgReviewsByItem, Item item) {
        Session session = HibernateAdapter.getSession();
        item.setRating(avgReviewsByItem);
        session.beginTransaction();
        session.save(item);
        session.getTransaction().commit();
    }
}
