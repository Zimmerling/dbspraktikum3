package repository;

import adapter.HibernateAdapter;
import model.Categorie;
import model.Item;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.Arrays;
import java.util.List;

public class CategorieRepository {

    public Categorie getCategoy(long id) {
        Session session = HibernateAdapter.getSession();
        return session.get(Categorie.class, id);
    }

    public List<Categorie> getCategories() {
        Session session = HibernateAdapter.getSession();
        Query<Categorie> query = session.createQuery("FROM Categorie", Categorie.class);
        return query.list();
    }

    public List<Categorie> getMainCategories() {
        Session session = HibernateAdapter.getSession();
        Query<Categorie> query = session.createQuery("From Categorie WHERE mainCategorie = true");
        return query.list();
    }

    public List<Item> getProductsByCategoryPath(String path) {
        path = path.replace("[\\s\\r]+", "");
        String[] categoryPath = Arrays.stream(path.split("/")).filter(s -> s.length() != 0).toArray(String[]::new);
        if (categoryPath.length == 0) {
            System.err.println("Category not found");
        }

        Categorie categorie = findCategorieRecursive(new CategorieRepository().getMainCategories(), 0, categoryPath);
        return new ItemRepository().getProductsByCategoryId(categorie.getId());
    }

    private Categorie findCategorieRecursive(List<Categorie> mainCategories, int i, String[] categoryPath) {
        return null;
    }

}
