package repository;

import adapter.HibernateAdapter;
import model.Item;
import model.Stock;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class StockRepository {


    public List<Stock> getStockByItem(Item item) {
        Session session = HibernateAdapter.getSession();
        Query<Stock> query = session.createQuery("SELECT s FROM Stock s WHERE s.item = :item and s.price > 0", Stock.class);
        query.setParameter("item", item);
        return query.list();


    }
}
