package repository;

import adapter.HibernateAdapter;
import model.Item;
import model.Review;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ReviewRepository {
    public void addNewReview(Review review) {
        Session session = HibernateAdapter.getSession();
        session.beginTransaction();
        session.save(review);
        session.getTransaction().commit();
    }

    public List<String> getTrolls(Double trollRating){
        Session session = HibernateAdapter.getSession();
        Query<String> query = session.createQuery("SELECT r.benutzer FROM Review r GROUP BY r.benutzer HAVING avg(r.rating)<:trollRating", String.class);
        query.setParameter("trollRating", trollRating);
        return query.list();
    }

    public float getAvgReviewsByItem(Item item) {
        Session session =HibernateAdapter.getSession();
        Query<Double> query = session.createQuery("SELECT avg(r.rating) FROM Review r where r.item = :item", Double.class);
        query.setParameter("item", item);
        return query.getSingleResult().floatValue();
    }
}
