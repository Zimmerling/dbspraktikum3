package exception;

public class AmbiguousCategoryNameException extends Exception{
    public AmbiguousCategoryNameException(String[] path, int index){
        super(String.format("Der Categoriepfad: '%s' ist leider Mehrdeutig mmit dem Categorienaamen: '%s'", String.join("/", path), path[index]));
    }
}
