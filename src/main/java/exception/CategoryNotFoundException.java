package exception;

public class CategoryNotFoundException extends Exception {

    public CategoryNotFoundException(String[] path, int index) {
        super(String.format("Es wurde kein Kategorie mit dem Namen: '%s' in dem Pfad: '%s' gefunden", path[index], String.join("/", path)));
    }

    public CategoryNotFoundException(String[] path) {
        super(String.format("Der Pfad: '%s' wurde nicht gefunden", String.join("/", path)));
    }
}
