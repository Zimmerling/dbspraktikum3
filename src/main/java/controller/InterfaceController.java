package controller;

import exception.AmbiguousCategoryNameException;
import exception.CategoryNotFoundException;
import model.Item;
import model.Stock;

import java.util.List;

public interface InterfaceController {
    public void init();
    public void finish();
    public Item getProduct(long pattern);
    public List<Item> getProducts(String pattern);
    public void getCategoryTree();
    public List<Item> getProductsByCategoryPath (String categoryPath) throws CategoryNotFoundException, AmbiguousCategoryNameException;
    public List<Item> getTopProducts(int i);
    public List<Item> getSimilarCheaperProduct(long id);
    public void addNewReview(String username, int id, int stars, String content, String summary);
    public List<Stock> getOffers(long id);
    public List<String> getTrolls(double trollrating);
}
