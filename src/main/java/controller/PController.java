package controller;

import adapter.HibernateAdapter;
import model.Categorie;
import model.Item;
import model.Review;
import model.Stock;
import repository.CategorieRepository;
import repository.ItemRepository;
import repository.ReviewRepository;
import exception.AmbiguousCategoryNameException;
import exception.CategoryNotFoundException;
import repository.StockRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PController implements InterfaceController {

    ItemRepository itemRepository = new ItemRepository();
    CategorieRepository categorieRepository = new CategorieRepository();
    ReviewRepository reviewRepository = new ReviewRepository();
    StockRepository stockRepository = new StockRepository();

    @Override
    public void init() {
        HibernateAdapter.initSessionFactory();
        System.out.println("fertig");
        HibernateAdapter.initSession();
    }

    @Override
    public void finish() {
        HibernateAdapter.finishSessions();
        HibernateAdapter.finishSessionFactory();
    }

    @Override
    public Item getProduct(long id) {
        return itemRepository.getProduct(id);
    }

    @Override
    public List<Item> getProducts(String pattern) {
        return itemRepository.getProducts(pattern);
    }

    @Override
    public void getCategoryTree() {
        Categorie godCategory = new Categorie("GodCategorie");
        for (Categorie mainCategorie : categorieRepository.getMainCategories()) {
            godCategory.getSubcategories().add(mainCategorie);
            try {
                File categoryTree = new File("categoryTree.txt");
                FileWriter fw = new FileWriter("categoryTree.txt");
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(godCategory.toTreeString(0));
                bw.close();
            } catch (Exception e) {
            }
            System.out.println(godCategory.toTreeString(0));
        }
    }

    @Override
    public List<Item> getProductsByCategoryPath(String categoryPath) throws CategoryNotFoundException, AmbiguousCategoryNameException {
        String[] path = categoryPath.toLowerCase().replaceAll("[\\s\\r]+", "").split("/");
        if (path.length == 0) throw new CategoryNotFoundException(path);
        Categorie categorie = findCategoryRecursive(new CategorieRepository().getMainCategories(), 0, path);
        return categorie.getItems().stream().toList();
    }

    private Categorie findCategoryRecursive(List<Categorie> mainCategories, int index, String[] path) throws AmbiguousCategoryNameException, CategoryNotFoundException {
        List<Categorie> matches = mainCategories.stream()
                .filter(categorie -> categorie.getName().toLowerCase().replaceAll("[\\s\\r]+", "").equals(path[index]))
                .collect(Collectors.toList());
        if (matches.isEmpty()){
            throw new CategoryNotFoundException(path, index);
        }else if(matches.size()>1){
            throw new AmbiguousCategoryNameException(path, index);
        }
        Categorie match = matches.get(0);
        if (index == path.length-1){
            return match;
        }
        return findCategoryRecursive(match.getSubcategories().stream().toList(), index+1, path);
    }

    @Override
    public List<Item> getTopProducts(int i) {
        return itemRepository.getTopProducts().subList(0, i);

    }

    @Override
    public List<Item> getSimilarCheaperProduct(long id) {
        List<Item> similarItems = itemRepository.getProduct(id).getSimilars().stream().toList();
        List<Item> cheaperItems = itemRepository.getCheaperItems(id);
        List<Item> ret = new ArrayList<>();
        if (cheaperItems != null && similarItems != null) {
            for (Item item : similarItems) {
                if (cheaperItems.contains(item)) {
                    ret.add(item);
                }
            }
        }
        return ret;
    }

    @Override
    public void addNewReview(String username, int id, int stars, String content, String summary) {
        Review review = new Review();
        review.setBenutzer(username);
        review.setContent(content);
        if (stars > 0 && stars < 6) {
            review.setRating(stars);
        } else throw new NumberFormatException();
        review.setReviewdate(new Date());
        Item item = itemRepository.getProduct(id);
        if (item != null) {
            review.setItem(item);
        }
        review.setSummary(summary);
        reviewRepository.addNewReview(review);
        recalculateAvgRatingByItem(review.getItem());
    }

    private void recalculateAvgRatingByItem(Item item) {
        itemRepository.setRatingAvgByItem(reviewRepository.getAvgReviewsByItem(item), item);
    }

    @Override
    public List<Stock> getOffers(long id) {
        Item item = itemRepository.getProduct(id);
        return stockRepository.getStockByItem(item);
    }

    @Override
    public List<String> getTrolls(double trollrating) {
        return reviewRepository.getTrolls(trollrating);
    }
}
