package adapter;

import lombok.NoArgsConstructor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
@NoArgsConstructor
public class HibernateAdapter {



    private static SessionFactory sessionFactory;
    private static final Session[] sessions = new Session[1];

    public static void initSessionFactory() {
        final StandardServiceRegistry reg = new StandardServiceRegistryBuilder().configure().build();
        try {
            sessionFactory = new MetadataSources(reg).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            StandardServiceRegistryBuilder.destroy(reg);
        }

    }

    public static void initSession() {
        sessions[0] = sessionFactory.openSession();
    }

    public static void finishSessionFactory() {
        sessionFactory.close();
    }

    public static void finishSessions() {
        for (Session session : sessions) {
            if (session != null) session.close();
        }
    }

    public static Session getSession() {
        return sessions[0];
    }

}



