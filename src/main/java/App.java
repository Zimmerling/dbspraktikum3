
import controller.InterfaceController;
import controller.PController;
import exception.AmbiguousCategoryNameException;
import exception.CategoryNotFoundException;
import model.Item;
import model.Stock;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        InterfaceController controller = new PController();
        commandLoop(controller, scanner);
    }

    private static void commandLoop(InterfaceController controller, Scanner scanner) {
        boolean isInitialized = false;
        out:
        while (true) {
            if (isInitialized == false) {
                System.out.println("Zuerst muss die Datenbank initialisiert werden (gib \"init\" ein)");
                if (scanner.nextLine().toLowerCase().equals("init")) {
                    controller.init();
                    isInitialized = true;
                }
            } else switch (scanner.nextLine().toLowerCase().replace(" ", "")) {
                case "init":
                    System.out.println("ist schon initialisiert");
                    break;
                case "getproduct":
                    getProduct(controller, scanner);
                    break;
                case "getproducts":
                    getProducts(controller, scanner);
                    break;
                case "getcategorytree":
                    controller.getCategoryTree();
                    break;
                case "getproductsbycategorypath":
                    getProductsByCategoryPath(controller, scanner);
                    break;
                case "gettopproducts":
                    getTopProducts(controller, scanner);
                    break;
                case "getsimilarcheaperproduct":
                    getSimilarCheaperproduct(controller, scanner);
                    break;
                case "addnewreview":
                    addNewReview(scanner, controller);
                    break;
                case "gettrolls":
                    getTrolls(scanner, controller);
                    break;
                case "getoffers":
                    getOffers(scanner, controller);
                    break;
                case "finish":
                    controller.finish();
                    isInitialized = false;
                    System.out.println("Die Verbindung zu der Datenbank wurde erfolgreich getrennt");
                    break;
                case "help":
                    StringBuilder s = new StringBuilder();
                    s.append("init  ---  zum initialisieren der Datenbank").append("\n")
                            .append("finish   ---   zum trennen der Datenbankenverbindung").append("\n")
                            .append("getProduct   ---   gibt zu einer ID das jeweilige Produkt zurück").append("\n")
                            .append("getProducts   ---   liefert alle Produkte zurück").append("\n")
                            .append("getCategoryTree   ---   liefert alle Kategorienen in einer Baumstruktur zurück").append("\n")
                            .append("getProductsByCategoryPath   ---   liefert alle Produkte eines Kategoriepfades zurück").append("\n")
                            .append("getTopProducts   ---   liefert die k besten Produkte zurück").append("\n")
                            .append("getSimilarCheaperProduct   ---   liefert ähnliche und günstigere Produkte zurück").append("\n")
                            .append("addnewreview   ---   fügt ein neues Review für ein Produkt hinzu").append("\n")
                            .append("gettrolls   ---   gibt alle Trolls zurück").append("\n")
                            .append("getOffers   ---   gibt für ein Produkt alle Verfügbarkeiten zurück");
                    System.out.println(s.toString());
                    break;
                default:
                    System.out.println("diesen Befehl kenne ich leider nicht aber mit \"help\" werden dir alle Befehle angezeigt");
            }
            System.out.println("");
        }
    }

    private static void getProductsByCategoryPath(InterfaceController controller, Scanner scanner) {
        System.out.println("Gib einen Kategoriepfad ein:");
        try {
            for (Item item : controller.getProductsByCategoryPath(scanner.nextLine())) {
                System.out.println(item);
            }
        } catch (AmbiguousCategoryNameException ambiguousCategoryNameException) {
            System.out.println(ambiguousCategoryNameException.getMessage());
        } catch (CategoryNotFoundException categoryNotFoundException) {
            System.out.println(categoryNotFoundException.getMessage());
        }
    }

    private static void getSimilarCheaperproduct(InterfaceController controller, Scanner scanner) {
        int k;
        System.out.println("Gib eine Produkt-ID ein");
        try {
            k = Integer.parseInt(scanner.nextLine());
        } catch (Exception e) {
            System.out.println("Die Eingabe muss eine Zahl sein!");
            return;
        }
        List<Item> list = new ArrayList<>(controller.getSimilarCheaperProduct(k));
        if (list.size() > 0) {
            for (Item item : list) {
                System.out.println(item);
            }
        } else System.out.println("Es wurden keine ähnlichen und günstigeren Produkte gefunden");
    }

    private static void getTopProducts(InterfaceController controller, Scanner scanner) {
        int k;
        System.out.println("Wie viele Produkte sollen angezeigt werden?");
        try {
            k = Integer.parseInt(scanner.nextLine());
        } catch (Exception e) {
            System.out.println("Die Eingabe muss eine Zahl sein!");
            return;
        }
        for (Item item : controller.getTopProducts(k)) {
            System.out.println(item);
        }
    }

    private static void getProducts(InterfaceController controller, Scanner scanner) {
        System.out.println("Gib ein Pattern ein nach dem gesucht werden soll");
        String input = scanner.nextLine();
        if (input.length() <= 1) input = "";
        List<Item> items = controller.getProducts(input);
        for (Item item : items) {
            System.out.println("Das Produkt mit dem Titel: \"" + item.getTitle() + "\" der id: \"" + item.getId() + "\" und der PGroup: \"" + item.getPgroup() + "\" wurde gefunden.");
        }
        if (items.isEmpty()) {
            System.out.println("Es wurde kein Produkt mit diesem Pattern gefunden");
        }
    }

    private static void getProduct(InterfaceController controller, Scanner scanner) {
        System.out.println("Gib eine Produkt-ID ein um ein Produkt zu bekommen");
        try {
            int id = Integer.parseInt(scanner.nextLine());
            Item item = controller.getProduct((id));
            System.out.println("Das Produkt mit dem Titel: \"" + item.getTitle() + "\" der id: \"" + item.getId() + "\" und der PGroup: \"" + item.getPgroup() + "\" wurde gefunden.");
        } catch (Exception exception) {
            System.out.println("Es wurde kein Produkt mit dieser ID gefunden.");
        }
    }

    private static void getOffers(Scanner scanner, InterfaceController controller) {
        long id;
        System.out.println("Für welche Produkt-ID möchten sie alle verfügbaren Angebote angezeigt bekommen?");
        try {
            id = Integer.parseInt(scanner.nextLine());
        } catch (Exception exeption) {
            System.out.println("Bitte geben sie eine Zahl ein");
            return;
        }
        List<Stock> stock = controller.getOffers(id);
        try {
            System.out.println("Für die Produk-ID wurde das Produkt: \"" + stock.get(0).getItem().getTitle() + "\" gefunden");
            for (Stock s : stock) {
                StringBuilder stringBuilder = new StringBuilder();
                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                stringBuilder.append("Vorhanden in der Filiale ")
                        .append(s.getShop().getName())
                        .append(" (")
                        .append(s.getShop().getStreet())
                        .append(" " + s.getShop().getZip()).append(") und kostet ")
                        .append(decimalFormat.format(s.getPrice() * s.getMult()))
                        .append(" " + s.getCurrency());
                System.out.println(stringBuilder.toString());
            }
        } catch (Exception e) {
            System.out.println("Es wurde für das Produkt mit der Id " + id + " kein Vorkommen gefunden");
        }
    }

    private static void getTrolls(Scanner scanner, InterfaceController controller) {
        double trollrating;
        System.out.println("ab welchem Rating ist man ein Troll?");
        try {
            trollrating = Double.parseDouble(scanner.nextLine());
            if (trollrating < 1 || trollrating > 5) throw new Exception();
        } catch (Exception e) {
            System.out.println("Es sollte eine Zahl zwischen 1 und 5 eingegeben werden");
            return;
        }
        System.out.println("Das sind alle Benutzer, welche im Durchschnitt unter " + trollrating + " Punkte für ihre bewerteten Artikel vergeben haben \n");
        for (String troll : controller.getTrolls(trollrating)) {
            System.out.println(troll + " ist ein Troll");
        }


    }

    private static void addNewReview(Scanner scanner, InterfaceController controller) {
        int stars, id;
        System.out.println("gib deinen Nutzernamen ein:");
        String username = scanner.nextLine();
        try {
            System.out.println("für welche Item ID soll ein Review hinzugefügt werden?");
            id = Integer.parseInt(scanner.nextLine());
        } catch (Exception e) {
            System.out.println("Du solltest eine Zahl eingeben");
            return;
        }
        try {
            System.out.println("Wie viele Sterne gibst du dem Produkt? (1-5)");
            stars = Integer.parseInt(scanner.nextLine());
            if (stars > 5 || stars < 1) {
                System.out.println("Du solltest eine Zahl zwischen 1 und 5 eingeben");
                return;
            }
        } catch (Exception e) {
            System.out.println("Du solltest eine Zahl zwischen 1 und 5 eingeben");
            return;
        }
        System.out.println("gib deinen Zusammenfassung für das Produkt ein:");
        String summary = scanner.nextLine();
        System.out.println("gib den Inhalt für das Review ein:");
        String content = scanner.nextLine();
        controller.addNewReview(username, id, stars, content, summary);
        System.out.println("Das Review wurde erstellt");
    }
}
